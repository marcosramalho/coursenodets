export default function(sequelize, DataTypes) {
  const Post = sequelize.define('Post', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },    
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    text: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    authorId: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  });

  /* Post.sync({
    force: true
  });
 */
  return Post;
}