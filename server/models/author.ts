export default function (sequelize, DataTypes) {
  const Author = sequelize.define('Author', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    }
  });

 /*  Author.sync({
    force: true
  });
   */
  return Author;
}