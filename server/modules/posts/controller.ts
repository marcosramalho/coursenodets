import {Request, Response } from 'express';
import * as _ from 'lodash';

import handlers from '../../api/responses/handlers';
import Post from './service';

class PostController {

  getAll(req: Request, res: Response) {
    Post.getAll()
      .then(_.partial(handlers.onSuccess, res))
      .catch(_.partial(handlers.onError, res, 'Erro ao buscar todos os posts'));
  }

  createPost(req: Request, res: Response) {
    Post.create(req.body)
      .then(_.partial(handlers.onSuccess, res))
      .catch(_.partial(handlers.dbErrorHandler, res))
      .catch(_.partial(handlers.onError, res, 'Erro ao inserir novo post'));
  }

  getById(req: Request, res: Response) {
    const postId = parseInt(req.params.id);

    Post.getById(postId)
      .then(_.partial(handlers.onSuccess, res))      
      .catch(_.partial(handlers.onError, res, 'Erro ao busca post'));
  }

  updatePost(req: Request, res: Response) {
    const postId = parseInt(req.params.id);
    const props = req.body;

    Post.update(postId, props)
      .then(_.partial(handlers.onSuccess, res))      
      .catch(_.partial(handlers.onError, res, 'Erro ao editar post'));
  }

  deletePost(req: Request, res: Response) {
    const postId = parseInt(req.params.id);
    
    Post.delete(postId)
      .then(_.partial(handlers.onSuccess, res))      
      .catch(_.partial(handlers.onError, res, 'Erro ao deletar post'));
  }
}

export default new PostController();