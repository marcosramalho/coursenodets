import { Application, Request, Response } from 'express';
import UserRoutes from '../../modules/User/routes';
import TokenRoutes from '../../modules/auth/auth';
import AuthorRoutes from '../../modules/author/routes';
import PostRoutes from '../../modules/posts/routes';

class Routes {
  
  constructor() {}

  initRoutes(app: Application, auth: any): void {
    app.route('/api/users/all').all(auth.config().authenticate()).get(UserRoutes.index);
    app.route('/api/users/create').all(auth.config().authenticate()).post(UserRoutes.create);
    app.route('/api/users/:id').all(auth.config().authenticate()).get(UserRoutes.findOne);
    app.route('/api/users/:id/update').all(auth.config().authenticate()).put(UserRoutes.update);
    app.route('/api/users/:id/destroy').all(auth.config().authenticate()).delete(UserRoutes.destroy);
    app.route('/token').post(TokenRoutes.auth);

    app.route('/api/author/all').all().get(AuthorRoutes.index);
    app.route('/api/author/create').all().post(AuthorRoutes.create);
    app.route('/api/author/:id').all().get(AuthorRoutes.findOne);
    app.route('/api/author/:id/update').all().put(AuthorRoutes.update);
    app.route('/api/author/:id/destroy').all().delete(AuthorRoutes.destroy);

    app.route('/api/post/all').all().get(PostRoutes.index);
    app.route('/api/post/create').all().post(PostRoutes.create);
    app.route('/api/post/:id').all().get(PostRoutes.findOne);
    app.route('/api/post/:id/update').all().put(PostRoutes.update);
    app.route('/api/post/:id/destroy').all().delete(PostRoutes.destroy);

  }
}

export default new Routes();
